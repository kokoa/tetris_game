document.getElementById("hello_text").textContent = "はじめてのJavaScripts"

document.addEventListener("keydown",onKeyDown);

let count = 0;

let cells;

// ブロックのパターン
let blocks = {
    i:{
        class:"i",
        pattern:[
            [1,1,1,1]
        ]
    },
    o:{
        class:"o",
        pattern:[
            [1,1,],
            [1,1]
        ]
    },
    t:{
        class:"t",
        pattern:[
            [0,1,0],
            [1,1,1]
        ]
    },
    s:{
        class:"s",
        pattern:[
            [0,1,1],
            [1,1,0]
        ]
    },
    z:{
        class:"z",
        pattern:[
            [1,1,0],
            [0,1,1]
        ]
    },
    j:{
        class:"j",
        pattern:[
            [1,0,0],
            [1,1,1]
        ]
    },
    l:{
        class:"l",
        pattern:[
            [0,0,1],
            [1,1,1]
        ]
    }
}

loadTables();
setInterval(function() {
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(経過時間:" + count + "点数:" + score + ")";

    /*for (let row = 0;row < 2; row++){
        for (let col = 0;col < 10;col++){
            if (cells[row][col].className !== ""){
                alert("game over")
            }
        }
    }*/
    if (hasFallingBlock()){
        fallBlocks();
    }else{
        deleteRow();
        generateBlock();
    }
},1000);


/* 以下、関数宣言の部分 ------------------------------------------------------------------*/

function loadTables(){
    cells = [];
    let td_array = document.getElementsByTagName("td");
    let index = 0;
    for (let row = 0;row <20;row++){
        cells[row] =  [];
        for (let col=0;col<10;col++){
            cells[row][col] = td_array[index];
            index++;
        }
    }

}
   
function fallBlocks(){
    for (let col = 0;col < 10;col++){
        if (cells[19][col].blockNum === fallingBlockNum){
            isFalling = false;
            return;

        }
    }
    for (let row = 18;row >= 0; row--){
        for (let col=0; col<10; col++){
            if (cells[row][col].blockNum === fallingBlockNum){
                if (cells[row+1][col].className !== "" && cells[row+1][col].blockNum !== fallingBlockNum){
                    isFalling = false;
                    return;
                }
            }
        }
    }

    for (let row = 18;row >= 0; row--){
        for (let col = 0;col < 10; col++){
            if (cells[row][col].blockNum === fallingBlockNum){
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

let isFalling = false;
function hasFallingBlock() {

    return isFalling;
}

// 左右移動させる関数------------------------------------------------

function moveRight() {
    for (let row = 0; row < 20 ; row++){
        for (let col = 9; col >=0; col--){
            if (cells[row][col].blockNum === fallingBlockNum){
                cells[row][col+1].className = cells[row][col].className;
                cells[row][col+1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;

            }
        }
    }
}


function moveLeft() {
    for (let row = 0; row < 20 ; row++){
        for (let col = 0;col < 10; col++){
            if (cells[row][col].blockNum === fallingBlockNum){
                cells[row][col-1].className = cells[row][col].className;
                cells[row][col-1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;

            }
        }
    }
}


// 新規ブロック作成　＆　揃った列削除-------------------------------------
let fallingBlockNum = 0;

function generateBlock(){
    let keys = Object.keys(blocks);
    let nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    let nextBlock = blocks[nextBlockKey];
    let nextFallingBlockNum = fallingBlockNum +1;

    //新規ブロック作成場所に、既にブロックが存在する場合、game over表示。ポップアップ内のokを押すとページリロード
    let pattern = nextBlock.pattern;
    for (let row = 0;row < pattern.length;row++){
        for (let col = 0;col < pattern[row].length; col++){
            if (pattern[row][col]){
                if (cells[row][col + 3].className !== "" && cells[row][col + 3].blockNum !== null){
                    finishAndGameRestart();
                }else{
                    cells[row][col + 3].className = nextBlock.class;
                    cells[row][col + 3].blockNum = nextFallingBlockNum;
                }
            }
        }
    }
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}

function onKeyDown(event){
    if (event.keyCode === 37){
        moveLeft();
    } else if (event.keyCode === 39){
        moveRight();
    }
}

let score = 0;
function deleteRow(){
    for (var row = 19; row >= 0; row--){
        let canDelete = true;
        for (let col = 0;col < 10; col++){
            if (cells[row][col].className === ""){
                canDelete = false;
            }
        }
        
        if (canDelete){
            for (let col = 0;col < 10; col++){
                cells[row][col].className = "";
                score = score + 10;
            }
            //downRowが上手く更新されなかったので、for文内でdownRowを直接更新
            for (let downRow = row -1; downRow >= 0; downRow--){
                for (let col = 0; col < 10; col++){
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    //console.log(cells[downRow][col].className);
                    //console.log(cells[downRow][col].blockNum);
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                    //console.log(downRow);
                    //console.log(cells[downRow +1][col].className);
                    //console.log(cells[downRow+1][col].blockNum);

                    


                }
            }
        }
    }
}

//game over時の関数　点数を表示---------------------------------------

function finishAndGameRestart(){
    restart = confirm("Congratulations！！ You got " + score +" point!! \n Do you wanna play this game again?");
    if (restart){
        location.reload();
    }
}